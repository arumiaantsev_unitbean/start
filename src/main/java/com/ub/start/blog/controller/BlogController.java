package com.ub.start.blog.controller;

import com.ub.core.picture.model.PictureDoc;
import com.ub.core.picture.repository.PictureRepository;
import com.ub.start.blog.model.BlogDoc;
import com.ub.start.blog.repository.BlogRepository;
import com.ub.start.blog.view.search.SearchBlogRequest;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class BlogController {

    @Autowired
    private BlogRepository blogRepository;
    @Autowired private PictureRepository pictureRepository;

    //Отображение на странице
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(@ModelAttribute SearchBlogRequest request, Model model){
        //Установка колва отображающихся страниц
        request.setPageSize(4);
        model.addAttribute("search", blogRepository.findAll(request));

        return "com.ub.start.blog.index";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create( Model model){

        // СОздание типа записи
        BlogDoc blogDoc = new BlogDoc();
        blogDoc.setId(new ObjectId());
        blogDoc.setPicId(new ObjectId());
        model.addAttribute("doc", blogDoc);

        return "com.ub.start.blog.create";
    }

    //===========================================================================

    @RequestMapping(value = "/succes", method = RequestMethod.POST)
    public String create (@ModelAttribute BlogDoc blogDoc,
                         @RequestParam(required = false) MultipartFile pic, RedirectAttributes ra){

        PictureDoc pictureDoc = pictureRepository.save(blogDoc.getPicId(),pic);
        blogDoc.setPicId(pictureDoc.getId());
        blogDoc = blogRepository.save(blogDoc);
        ra.addAttribute("id", blogDoc.getId());
        return "com.ub.start.blog.succes";
    }

    //===========================================================================

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String news(@RequestParam ObjectId id,Model model){



        BlogDoc blogDoc =blogRepository.findById(id);
        model.addAttribute("doc", blogDoc);

        return "com.ub.start.blog.new";
    }

}
