package com.ub.start.blog.model;

import com.ub.core.base.model.BaseModel;
import com.ub.core.social.vk.api.response.users.get.UserInfo;
import org.springframework.data.mongodb.core.mapping.Document;
import org.bson.types.ObjectId;

import javax.persistence.Id;
import java.util.List;

@Document
public class BlogDoc extends BaseModel {

    @Id
    private ObjectId id;
    private String title;
    private String discription;
    private ObjectId picId;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public ObjectId getPicId() {
        return picId;
    }

    public void setPicId(ObjectId picId) {
        this.picId = picId;
    }


}