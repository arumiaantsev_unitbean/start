package com.ub.start.blog.view.search;

import com.ub.core.base.view.search.SearchRequest;

public class SearchBlogRequest extends SearchRequest {

//this.setPageSize(2); Означает что на одной странице навигации будут отображатся 2 объекта

    public SearchBlogRequest(){
    }

    public SearchBlogRequest(Integer currentPage){
        this.currentPage = currentPage;
    }

    public SearchBlogRequest(Integer currentPage,Integer pageSize){
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

}
