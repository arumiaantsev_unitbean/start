<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%--
 Created by IntelliJ IDEA.
 User: vladimir
 Date: 20.07.17
 Time: 10:54
 To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Create</title>
    <link href="static/start/css/style.css" rel="stylesheet"> </link>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
</head>
<body>


<div class="content mod-create">
    <c:set value="${doc}" var="doc" scope="request"/>
    <div class="blog-create">


        <form:form action="/succes" class="card-panel" modelAttribute="doc" method="post" enctype="multipart/form-data">
            <h1 class="blog-create-title">Добавление статьи</h1>

            <div class="blog-create-group">
                <lable class="blog-create-lable">Заголовок</lable>
                <form:input path='title' id='title' class="blog-create-input" type="text"/>
            </div>

            <div class="blog-create-group">
                <lable class="blog-create-lable">Описание</lable>
                <form:textarea path='discription' id='discription' class="blog-create-input mod-textarea"></form:textarea>
            </div>

            <div class="blog-create-footer">
                <lable class="blog-create-lable">Прикрепить изображение</lable>
                <div class="blog-create-file">
                    <input name="pic" type="file" class="blog-create-file-input">
                </div>
                <form:button class="btn btn-approve" type="submit" name="action">Добавить</form:button>
                <a href="/" class="btn btn-cancel">Отмена</a>
            </div>
        </form:form>
    </div>
</div>
</body>
</html>