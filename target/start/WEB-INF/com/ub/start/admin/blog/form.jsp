<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<form:hidden path="id"/>
<form:hidden path="picId"/>

<div class="row">
    <div class="input-field col s12">
        <form:input path="title" id="title"/>
        <label for="title">Заголовок</label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        <form:input path="discription" id="discription"/>
        <label for="title">Описание</label>
    </div>
</div>


<!-- Добавление и отображение картинок в админке-->
<div class="row">
    <div class="input-field col s12">
        <input type="file" name="pic">
        <label for="title"></label>
    </div>
</div>

<div class="row">
    <div class="input-field col s12">
        <img src="/pics/${doc.picId}" style="width: 100%">
    </div>
</div>