<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ftm" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>

    <link href="static/start/css/style.css" rel="stylesheet"> </link>
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
    <link href="static/ub/css/main.css" rel="stylesheet"> </link>
</head>
<div>

<div class="head">
    <div class="head-logo" style="background-image:url(static/start/img/лого.png)"></div>


    <div class="head-cout">Статьи,24</div>







  <a href="http://localhost:8080/create"><div style="background-image:url(static/start/img/статья.png)" class="head-blog-create"></div></a>

   </div>

<div class="content">

    <!-- Вывод на форму из массива данных -->

    <%--@elvariable id="blogs" type="java.util.List"--%>
    <c:forEach items="${search.result}" var="blog" varStatus="status">


        <%--<jsp:useBean id="new" scope="request"/>--%>

        <div class="blog-item">
            <img class="blog-item-background" src="/pics/${blog.picId}"/>

            <div class="blog-item-title"> ${blog.title}   </div>

            <!-- Форматирование вывода даты-->
            <div class="blog-item-date">  <ftm:formatDate pattern="dd.MM.yyyy" value="${blog.createdAt}"/></div>

            <div class="blog-item-hr"></div>

            <div class="blog-item-text"> ${blog.discription} </div>
            <c:url value="/new" var="editUrl">
                <c:param name="id" value="${blog.id}"/>
            </c:url>

            <a href="${editUrl}" class="arhive"> <div class="arhive">Подробнее</div> </a>
        </div>
    </c:forEach>

    <!-- ////////////////////////////////////////////////Пагинация//////////////////////////////////////////////////-->


    <div class="section">
        <div class="row center-align">
            <ul class="pagination">

                <!-- ////////////////////////////////////////////////Пагинация вперёд//////////////////////////////////////////////////-->

                <c:url value="/" var="prevUrl">
                    <c:param name="query" value="${search.query}"/>
                    <c:param name="currentPage" value="${search.prevNum}"/>
                </c:url>
                <li class="${search.prevNum eq search.currentPage ? 'disabled' : 'waves-effect'}">
                    <a <c:if test="${search.prevNum ne search.currentPage}">href="${prevUrl}"</c:if>>
                        <i class="mdi-navigation-chevron-left"></i>
                    </a>
                </li>

                <!-- forEach это сами цифры -->
                <c:forEach items="${search.pagination}" var="page">
                    <c:url value="/" var="pageUrl">
                        <c:param name="query" value="${search.query}"/>
                        <c:param name="currentPage" value="${page}"/>
                    </c:url>

                    <!-- Вывод пагинации -->


                    <li class="${search.currentPage eq page ? 'active' : ''}">
                        <a href="${search.currentPage ne page ? pageUrl : ''}">${page + 1}</a>
                    </li>
                    <!-- Конец Вывода пагинации -->
                </c:forEach>


                <!-- ////////////////////////////////////////////////Пагинация назад//////////////////////////////////////////////////-->

                <!-- Переключение следующей страницы и предыдущей -->
                <c:url value="/" var="nextUrl">
                    <c:param name="query" value="${search.query}"/>
                    <c:param name="currentPage" value="${search.nextNum}"/>
                </c:url>
                <!-- Конец Переключение следующей страницы и предыдущей -->

                <li>
                    <a <c:if test="${search.nextNum ne search.currentPage}">href="${nextUrl}"</c:if>>
                        <i class="mdi-navigation-chevron-right"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>




</div>
</body>
</html>
