<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="ftm" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>rqee3</title>
</head>



<c:set value="${doc}" var="doc" scope="request"/>
<body>

<div class="u">
    <img class="blog-item-background" src="/pics/${doc.picId}"/>

    <div class="blog-item-title"> ${doc.title}   </div>

    <!-- Форматирование вывода даты-->
    <div class="blog-item-date">  <ftm:formatDate pattern="dd.MM.yyyy" value="${doc.createdAt}"/></div>

    <div class="blog-item-hr"></div>

    <div class="blog-item-text"> ${doc.discription} </div>
</div>

</body>
</html>
